import React from "react";

const layout = (props) => {
    return (
        <>
            <main className='container clearfix'>{props.children}</main>
        </>
    )

};

export default layout;