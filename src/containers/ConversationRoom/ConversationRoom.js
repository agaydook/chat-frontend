import React, {Component} from "react";
import Message from '../../components/ConversationRoom/Messages/Message/Message';
import SearchBar from '../../components/ConversationRoom/SearchBar/SearchBar';
import NewMessage from '../../components/ConversationRoom/Messages/NewMessage/NewMessage';
import axios from '../../axios-chat';
import {ActionCable} from 'react-actioncable-provider';

class ConversationRoom extends Component {
    state = {
        searchResult: null,
        currentRoom: null,
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.currentRoomId) {
            if (!this.state.currentRoom || (this.state.currentRoom && (this.props.currentRoomId !== this.state.currentRoom.id))) {
                axios.get('/conversation_rooms/' + this.props.currentRoomId)
                    .then(response => {
                        this.setState({
                            currentRoom: response.data,
                            searchResult: null
                        });
                    })
                    .catch(error => console.log(error));
            }
        }
    }

    handleReceivedMessage = response => {
        const {message} = response;
        const currentRoom = this.state.currentRoom;
        const oldMessages = currentRoom.messages;

        currentRoom.messages = [...oldMessages, message];

        this.setState({currentRoom: currentRoom});
    };

    handleSearchRequest = (query) => {
        if (query) {
            const messages = this.state.currentRoom.messages;
            const filteredMessages = messages.filter(message => message.text.indexOf(query) > -1);
            this.setState({searchResult: filteredMessages})
        } else {
            this.setState({searchResult: null})
        }
    };

    render() {
        // TODO: SHOULD BE MOVED TO MESSAGE ITEMS COMPONENT

        let messageComponents = [];

        if (this.state.currentRoom) {
            let messages = this.state.searchResult ? this.state.searchResult : this.state.currentRoom.messages;

            messageComponents = messages.map(message => {
                return <Message
                    key={message.id}
                    text={message.text}
                    sentAt={message.sent_at}
                    createdBy={message.created_by}
                    handleReceivedMessage={this.handleReceivedMessage}/>
            });
        }

        return (
            <>
                {
                    this.state.currentRoom ? (
                        <>
                            <ActionCable
                                key={this.state.currentRoom.id}
                                channel={{channel: 'MessagesChannel', conversation_id: this.state.currentRoom.id}}
                                onReceived={this.handleReceivedMessage}
                            />
                            <SearchBar handleSearchRequest={this.handleSearchRequest}
                                       currentRoomId={this.state.currentRoom.id}/>
                            <div className='chat-history'>
                                <ul>
                                    {messageComponents}
                                </ul>
                            </div>
                            <NewMessage currentRoomId={this.state.currentRoom.id}
                                        currentUser={this.props.currentUser}/>
                        </>
                    ) : (
                        <div>
                            <h3 style={{color: 'white', textAlign: 'center'}}>Please select conversation room or create your own</h3>
                        </div>
                    )
                }
            </>
        );
    }
}

export default ConversationRoom;
