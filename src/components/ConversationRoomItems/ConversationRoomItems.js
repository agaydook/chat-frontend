import React from 'react';
import ConversationRoomItem from './ConversationRoomItem/ConversationRoomItem';

const conversationRoomItems = (props) => (
    props.conversationRooms.map(room => {
        return <ConversationRoomItem
            key={room.id}
            id={room.id}
            currentRoomId={props.currentRoomId}
            name={room.name}
            joinToRoom={() => props.joinToRoom(room.id)}/>
    })
);

export default conversationRoomItems;