import React, {Component} from "react";
import ConversationRoomItems from '../../components/ConversationRoomItems/ConversationRoomItems';
import ConversationRoom from '../ConversationRoom/ConversationRoom';
import NewConversationRoom from '../../components/NewConversationRoom/NewConversationRoom';
import axios from '../../axios-chat';
import {ActionCable} from 'react-actioncable-provider';
import LoginForm from '../../components/LoginForm/LoginForm';

class Chat extends Component {
    state = {
        conversationRooms: [],
        messages: [],
        searchResult: [],
        currentRoomId: null,
        currentUser: null
    };

    componentDidMount() {
        axios.get('/conversation_rooms')
            .then(response => this.setState({conversationRooms: response.data}))
            .catch(error => console.log(error));
    }

    joinToRoom = (id) => {
        this.setState({currentRoomId: id});
        axios.get('/conversation_rooms/' + id + '/messages')
            .then(response => this.setState({messages: response.data}))
            .catch(error => console.log(error));
    };

    signIn = (currentUser) => {
        this.setState({currentUser: currentUser});
    };

    handleReceivedConversationRoom = response => {
        const {conversation_room} = response;
        const conversationRooms = this.state.conversationRooms;
        this.setState({conversationRooms: [...conversationRooms, conversation_room]});
    };

    handleSearchRequest = query => {
        const messages = this.state.messages;
        const filteredMessages = messages.filter(message => message.text.indexOf(query) > -1);
        this.setState({searchResult: filteredMessages})
    };

    render() {
        return (
            <>
                <ActionCable
                    channel={{channel: 'ConversationRoomsChannel'}}
                    onReceived={this.handleReceivedConversationRoom}
                />

                {this.state.currentUser ? (
                    <div>
                        <div className='room-list'>
                            <NewConversationRoom/>
                            <ul className="list">
                                <ConversationRoomItems
                                    conversationRooms={this.state.conversationRooms}
                                    currentRoomId={this.state.currentRoomId}
                                    joinToRoom={this.joinToRoom}/>
                            </ul>
                        </div>

                        <div className='chat'>
                            <ConversationRoom
                                currentRoomId={this.state.currentRoomId}
                                currentUser={this.state.currentUser}/>
                        </div>
                    </div>
                ) : <LoginForm signIn={this.signIn}/>}
            </>
        );
    }
}

export default Chat;