import React, {Component} from 'react';

class LoginForm extends Component {
    state = {
        currentUser: ''
    };

    render() {
        return (
            <div style={{margin: '0 auto', width: '300px', padding: '30px'}}>
                <h2>Welcome to chat</h2>
                <p>Please, enter your name to join conversation room</p>
                <input
                    maxLength={120}
                    type='text'
                    value={this.state.currentUser}
                    onChange={(event) => this.setState({currentUser: event.target.value})}/>
                <button
                    onClick={() => {this.props.signIn(this.state.currentUser)}}
                    disabled={!this.state.currentUser}>
                    Sign In
                </button>
            </div>
        );
    }
}

export default LoginForm;