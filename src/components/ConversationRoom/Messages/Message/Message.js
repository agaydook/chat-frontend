import React from 'react';

const message = (props) => (
    <li className="clearfix">
        <div className="message-data align-right">
            <span className="message-data-time">{props.sentAt}</span> &nbsp; &nbsp;
            <span className="message-data-name">{props.createdBy}</span>

        </div>
        <div className="message other-message float-right">
            {props.text}
        </div>
    </li>
);

export default message;