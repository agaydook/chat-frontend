import React, {Component} from 'react';
import axios from '../../axios-chat';

class NewConversationRoom extends Component {
    state = {
        name: ''
    };

    createConversationRoom = () => {
        const NewConversationRoom = {
            conversation_room: {name: this.state.name}
        };

        axios.post('/conversation_rooms/', NewConversationRoom)
            .then(response => {
                console.log('ok')
            })
            .catch(error => {
                console.log(error)
            })
    };

    render() {
        return (
            <div className='new-conversation'>
                <input
                    maxLength={120}
                    type='text'
                    placeholder='New Room'
                    value={this.state.name}
                    onChange={(event) => this.setState({name: event.target.value})}/>
                <button
                    onClick={this.createConversationRoom}
                    disabled={!this.state.name}>
                    Add
                </button>
            </div>
        );
    }
}

export default NewConversationRoom;