import React, {Component} from 'react';
import Layout from './components/Layout/Layout';
import Chat from './containers/Chat/Chat';
import {ActionCableProvider} from 'react-actioncable-provider';

//TODO: MUST BE REFACTORED
class App extends Component {
    render() {
        return (
            <ActionCableProvider url='ws://localhost:3001/cable'>
                <Layout>
                    <Chat/>
                </Layout>
            </ActionCableProvider>
        );
    }
}

export default App;
