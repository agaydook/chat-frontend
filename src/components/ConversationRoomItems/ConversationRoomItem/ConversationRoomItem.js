import React from 'react';

const conversationRoomItem = (props) => {
    let classes = ['clearfix', 'list-item'];

    if(props.currentRoomId === props.id)
        classes.push('active');

    return (
        <li className={classes.join(' ')} onClick={props.joinToRoom}>
            {props.name}
        </li>
    )
};

export default conversationRoomItem;