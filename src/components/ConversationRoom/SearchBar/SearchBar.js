import React, {Component} from 'react';

class SearchBar extends Component {
    state = {
        query: ''
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.currentRoomId !== this.props.currentRoomId)
            this.refs.searchInput.value=''
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.currentRoomId !== this.props.currentRoomId) ||
               (nextState.query !== this.state.query)
    }

    doSearchRequest = (query) => {
        this.setState({query: query});
        this.props.handleSearchRequest(query);
    };

    render() {
        return (
            <>
                <div className='search-box'>
                    <input
                        maxLength={2000}
                        ref='searchInput'
                        type='text'
                        value={this.state.query}
                        onChange={(event) => this.doSearchRequest(event.target.value)}
                        placeholder='Search by messages'
                        style={{width: '100%'}}/>
                </div>
            </>
        );
    }
}

export default SearchBar;