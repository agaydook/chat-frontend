import React, {Component} from 'react';
import styles from './NewMessage.module.css';
import axios from '../../../../axios-chat';

class NewMessage extends Component {
    state = {
        text: ''
    };

    createMessage = () => {
        const newMessage = {
            message: {
                text: this.state.text,
                created_by: this.props.currentUser
            }
        };

        axios.post('/conversation_rooms/' + this.props.currentRoomId + '/messages', newMessage)
            .then(response => {
                this.setState({text: ''});
                this.refs.messageInput.focus();
            })
            .catch(error => {
                console.log(error)
            })
    };

    render() {
        return (
            <>
                <div className="chat-message clearfix">
                        <textarea
                            maxLength={2000}
                            style={{width: '100%'}}
                            ref="messageInput"
                            rows="4"
                            placeholder='New Message'
                            value={this.state.text}
                            onChange={(event) => this.setState({text: event.target.value})}/>
                    <button
                        style={{width: '100%'}}
                        onClick={this.createMessage}
                        disabled={!this.state.text}>
                        Add Message
                    </button>

                </div>
            </>
        );
    }
}

export default NewMessage;